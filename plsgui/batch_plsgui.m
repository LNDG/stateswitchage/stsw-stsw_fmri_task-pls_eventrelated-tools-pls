function batch_plsgui(varargin)
%% start PLS analysis or PLS datamat file creation.
% hacked version 2.0: receive input from both .mat and .txt 


   if nargin < 1
      error('Usage: batch_plsgui(batch_text_file_name(s))');
   end

   % initalize PLS
   p = which('plsgui');
   [p f] = fileparts(p); [p f] = fileparts(p);
   cmdloc = fullfile(p, 'plscmd');
   addpath(cmdloc);

   % Decide what file type % LP 20.03.2019
   if strcmpi(varargin{1}(end-2:end), 'mat')
       file_type = 1; % for .mat files
   elseif strcmpi(varargin{1}(end-2:end), 'txt')
       file_type = 0; % for .txt files
   else
       msg = 'Undefined file type. Input file should be either .mat or .txt.';
       error(msg);
   end
   
   %% Handle the input data depending on the file type % LP 20.03.2019
   
   if file_type % in case of .mat input
   % loop over various input files
   
   for i = 1:nargin
      load(varargin{i});
            
      % test if analysis or datamat creation has to be started 
      if batch_file.is_analysis==true
         batch_pls_analysis(batch_file, file_type); % LP 20.03.2019
      else
          batch_create_datamat(batch_file, file_type); % LP 20.03.2019
      end
   end

   return;					% batch_plsgui
   
   else % in case of .txt input       
      % loop over various input files
      for i = 1:nargin
      batch_file = varargin{i};
      fid = fopen(batch_file);

      tmp = fgetl(fid);

      if ischar(tmp) & ~isempty(tmp)
         tmp = strrep(tmp, char(9), ' ');
         tmp = deblank(fliplr(deblank(fliplr(tmp))));
      end

      while ~feof(fid) & (isempty(tmp) | isnumeric(tmp) | strcmpi(tmp(1), '%'))
         tmp = fgetl(fid);

         if ischar(tmp) & ~isempty(tmp)
            tmp = strrep(tmp, char(9), ' ');
            tmp = deblank(fliplr(deblank(fliplr(tmp))));
         end
      end

      fseek(fid, 0, 'bof');

      if ischar(tmp) & ~isempty(tmp)
         tok = strtok(tmp);
      else
         tok = '';
      end
      
      % test if analysis or datamat creation has to be started
      if strcmpi(tok, 'result_file')
         batch_pls_analysis(fid, file_type); % LP 20.03.2019
      else
         batch_create_datamat(fid, file_type); % LP 20.03.2019
      end
   end

   return;					% batch_plsgui

   end

